import React, { Component, Fragment } from 'react';
import Helmet from 'react-helmet';
import { Link } from 'react-router-dom';
import Icon from '@material-ui/core/Icon';
import CircularProgress from '@material-ui/core/CircularProgress';

import { title } from '../utils';
import { userService } from '../services';
import Article from '../components/Article';
import InfosUser from '../components/InfosUser';
import ErrorMessage from '../components/ErrorMessage';

class UserPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            list: null,
            id: null,
            user: null
        };
    }

    componentDidMount() {
        this.getUsers();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.state.id !== prevState.id) {
            this.setState({
                isLoading: true
            });
            this.getUserInfos(this.state.id);
        }
    }

    handleChange = (event) => {
        this.props.history.push(`/users/${event.target.value}`);
        this.setState({
            id: event.target.value
        });
    };

    getUsers = async () => {
        const list = await userService.list();
        if (Array.isArray(list) && list.length > 0) {
            this.setState({
                list,
                id: this.props.match.params.id ? this.props.match.params.id : 1
            });
        } else {
            this.setState({
                isLoading: false
            });
        }
    };

    getUserInfos = async (id) => {
        const user = await userService.infosUser(id);
        if (!user.status) {
            this.setState({
                isLoading: false,
                id,
                user
            });
        } else {
            this.setState({
                isLoading: false,
                id: null,
                user: null
            });
        }

    };

    render() {
        return (
            <Fragment>
                <Helmet>
                    { title('Page secondaire') }
                </Helmet>

                <div className="user-page content-wrap">
                    <Link to="/" className="nav-arrow">
                        <Icon>arrow_right_alt</Icon>
                    </Link>
                    { this.state.isLoading
                        ? <CircularProgress color="primary" size="200px" className="loading"/>
                        : (
                            !this.state.isLoading && this.state.list ?
                                <Fragment>
                                    <div className="users-select">
                                        <h1>
                                            <select value={this.state.id || ''} onChange={this.handleChange}>
                                                {this.state.list.map((user) => <option value={user.id} key={user.id}> {user.name} </option>)}
                                            </select>
                                        </h1>
                                    </div>
                                    <InfosUser user={this.state.user}/>
                                </Fragment>
                                : <ErrorMessage message="La liste des utilisateurs n'a pas pu être recuperé"/>
                        )
                    }
                    {(!this.state.isLoading && this.state.user && this.state.user.articles) &&
                        <div className="articles-list">{ this.state.user.articles.map((article) => <Article article={article} key={article.id}/>)}</div>
                    }
                    {(!this.state.isLoading && !this.state.user) &&
                    <ErrorMessage message="Les informations de l'utilisateur n'ont pas pu être recuperé"/>
                    }
                </div>
            </Fragment>
        );
    }
}

export default UserPage;
