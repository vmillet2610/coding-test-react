import React, { Component } from "react";

class Article extends Component{
    render() {
        return (
            <div className="article" key={this.props.article.id}>
                <span>{this.props.article.name}</span>
                <p>{this.props.article.content}</p>
            </div>
        )
    }
}

export default Article;
