// Dans les autres fichiers :
// import { Component } from 'components';

import ErrorMessage from './ErrorMessage';
import InfosUser from './InfosUser';
import Article from './Article';

export { ErrorMessage, InfosUser, Article };
