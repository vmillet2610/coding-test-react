import React, { Component } from "react";

function Occupation(props) {
    const occupation = props.value ? props.value : null;
    return occupation && <p>Occupation: <span>{occupation}</span></p>;
}

function BirthDate(props) {
    const options = {year: "numeric", month: "long", day: "2-digit"};
    const birthDate = props.value ? getFormatDate(props.value).toLocaleDateString("fr-CA", options) : null;
    return birthDate && <p>Date de naissance: <span>{birthDate}</span></p>;
}

function getFormatDate(dateString) {
    return new Date(dateString);
}

class InfosUser extends Component{
    render(){
        if (this.props.user) {
            return (
                <div className="infos-block">
                    <Occupation value={this.props.user.occupation}/>
                    <BirthDate value={this.props.user.birthdate}/>
                </div>
            )
        } else {
            return null;
        }
    }
}

export default InfosUser;
